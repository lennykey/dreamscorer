# Create your views here.
from django.http import HttpResponse
from django.template import Context, loader
from scorer.models import Scorer, Spieler, Item


def index(request):
    scorer_objects = Scorer.objects.all()
    spieler_objects = Spieler.objects.all()
    print(spieler_objects)

    mylist = [] 

    for spieler in spieler_objects:
        spielerList = []
        name = spieler.name 
        vorname = spieler.vorname 
        # print(name)
        # print(vorname)
        scorer_objects_tore = Scorer.objects.filter(spieler__name__exact=spieler.name).filter(spieler__vorname__exact=spieler.vorname).filter(item__itemtype__exact='Tor')
        scorer_objects_rote = Scorer.objects.filter(spieler__name__exact=spieler.name).filter(spieler__vorname__exact=spieler.vorname).filter(item__itemtype__exact='Rote Karte')
        scorer_objects_gelbe = Scorer.objects.filter(spieler__name__exact=spieler.name).filter(spieler__vorname__exact=spieler.vorname).filter(item__itemtype__exact='Gelbe Karte')
        scorer_objects_scores = Scorer.objects.filter(spieler__name__exact=spieler.name).filter(spieler__vorname__exact=spieler.vorname).filter(item__itemtype__exact='Score')

        spielerList.append(name)
        spielerList.append(vorname)


        summe_tore = sumObjects(scorer_objects_tore)
        summe_rote = sumObjects(scorer_objects_rote)
        summe_gelbe = sumObjects(scorer_objects_gelbe)
        summe_scores = sumObjects(scorer_objects_scores)

        spielerList.append(summe_tore)
        spielerList.append(summe_rote)
        spielerList.append(summe_gelbe)
        spielerList.append(summe_scores)

        mylist.append(spielerList)


        #print(scorer.anzahl)

        #mylist.append(name + tore)

    # scorer_objects = Scorer.objects.filter(item__itemtype__exact='Tor')
    # lenny = scorer_objects.filter(Spieler.name='lenny')
    # spieler_objects = Scorer.spieler.all()
    # print spieler_objects.name

    t = loader.get_template('index.html')
    c = Context({
        # 'scorer_objects': scorer_objects,
        # 'spieler_objects': spieler_objects,
        'mylist' : mylist,
    })
    return HttpResponse(t.render(c))


def sumObjects(scorer_objects):
    anzahl = 0
    for scorer_item in scorer_objects:
        anzahl += scorer_item.anzahl

    return anzahl
