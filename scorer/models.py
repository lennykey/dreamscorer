from django.db import models

# Create your models here.


class Spieler(models.Model):
    name = models.CharField(max_length=200)
    vorname = models.CharField(max_length=200)

    def __unicode__(self):
        return u"{vorname} {nachname}".format(vorname=self.vorname, nachname=self.name)

class Begegnung(models.Model):
    gegner = models.CharField(max_length=200)
    spieltyp = models.CharField(max_length=200)
    date_begegnung = models.DateTimeField('spielbegegnung')

    def __unicode__(self):
        datum = str(self.date_begegnung)

        return u"{gegner} {spieltyp} {datum}".format(gegner=self.gegner, spieltyp=self.spieltyp, datum=datum[0:11])

class Item(models.Model):
    itemtype = models.CharField(max_length=200)
    # anzahl = models.IntegerField()

    def __unicode__(self):
       return u"{item}".format(item=self.itemtype)

class Scorer(models.Model):
    spieler = models.ForeignKey(Spieler)
    begegnung = models.ForeignKey(Begegnung) 
    item = models.ForeignKey(Item)
    anzahl = models.IntegerField()

    def __unicode__(self):
       return u"{spieler} {begegnung} {item}".format(spieler=self.spieler, begegnung=self.begegnung, item=self.item)
