from scorer.models import Scorer, Item, Spieler, Begegnung
from django.contrib import admin

admin.site.register(Scorer)
admin.site.register(Item)
admin.site.register(Spieler)
admin.site.register(Begegnung)
